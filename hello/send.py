#coding:utf-8
__author__ = 'db'

import pika
import time
connection = pika.BlockingConnection(pika.ConnectionParameters(
        host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='hello')

i = 0
while 1:
    i+=1
    channel.basic_publish(exchange='',
                      routing_key='hello',
                      body='Hello World! %s' % i)
    print " [x] Sent 'Hello World! %s'" % i
    time.sleep(0.1)
connection.close()